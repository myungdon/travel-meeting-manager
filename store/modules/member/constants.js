export default {
    DO_MEMBER_LIST: 'member/doList', // 리스트를 불러오는 DO
    DO_MEMBER_DETAIL: 'member/doDetail', //리스트에 있는 회원의 상세정보를 불러오는 DO
    DO_MEMBER_DELETE: 'member/doDelete',// 리스트에 있는 회원정보를 삭제하는 DO
}
