const BASE_URL = '/v1/member'

export default {
    DO_MEMBER_LIST: `${BASE_URL}/all`, //get
    DO_DETAIL: `${BASE_URL}/{id}`, //get
    DO_DELETE: `${BASE_URL}/{id}`, //del
}
