import axios from 'axios'
import apiUrls from './api-urls'
import Constants from './constants'

export default {
    [Constants.DO_MEMBER_LIST]: (store) => {
        return axios.get(apiUrls.DO_LIST)
    },
    [Constants.DO_DETAIL]: (store, payload) => {
        return axios.get(apiUrls.DO_DETAIL.replace('{id}', payload.id))
    },
    [Constants.DO_DELETE]: (store, payload) => {
        return axios.delete(apiUrls.DO_DELETE.replace('{id}', payload.id))
    },
}
